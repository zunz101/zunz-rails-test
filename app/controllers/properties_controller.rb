class PropertiesController < ApplicationController
  before_action :set_property, only: [:show, :edit, :update, :destroy, :sell]
  before_action :authenticate_user!

  def sell
    message = ''
    success = false
    respond_to do |format|
      missing_fields = get_missing_fields(@property)
      if !can_sell(current_user)
        message = 'you have reach your plan limit'
        #format.html { redirect_to properties_url, notice: 'You have reach your plan limit to sell houses.' }  
      elsif missing_fields.empty? 
        
        if @property.update_attribute(:status, 1)
          success = true
          message = 'Property was set to forsale successfully.'
          #format.html { redirect_to properties_url, notice: 'Property was set to forsale successfully.' }
        else
          message = 'Unable to continue process.'
          #format.html { redirect_to @property, notice: 'Unable to continue process.' }
        end
      else
        message = 'Property must be 100% updated in order to sale.'
        #format.html { redirect_to @property, notice: 'Property must be 100% updated in order to sale.' }
      end

      format.html {redirect_to properties_url, notice: message } 
    end
  end  


  # GET /properties
  # GET /properties.json
  def index
      @properties = Property.where(user_id: current_user.id).order("created_on DESC")
  
      #can_sell(current_user)
  end

  # GET /properties/1
  # GET /properties/1.json
  def show
    #byebug 
    missing_fields = get_missing_fields(@property)
    percent = get_percentage(missing_fields, 4)
    @property['metas'] = {
      'missing_fields' => missing_fields,
      'percent' => percent 
    }

  end

  # GET /properties/new
  def new
    @property = Property.new
  end

  # GET /properties/1/edit
  def edit
    if @property.user != current_user
      redirect_to properties_url, alert: 'You are not allowed to edit this property.' 
    end  
  end

  # POST /properties
  # POST /properties.json
  def create

    @property = Property.new(property_params)
    @property.user_id = current_user.id

    respond_to do |format|
      if @property.save
        format.html { redirect_to @property, notice: 'Property was successfully created.' }
        format.json { render :show, status: :created, location: @property }
      else
        format.html { render :new }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /properties/1
  # PATCH/PUT /properties/1.json
  def update
    respond_to do |format|
      # byebug 
      if @property.update(property_params)
        format.html { redirect_to @property, notice: 'Property was successfully updated.' }
        format.json { render :show, status: :ok, location: @property }
      else
        format.html { render :edit }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/1
  # DELETE /properties/1.json
  def destroy
    @property.destroy
    respond_to do |format|
      format.html { redirect_to properties_url, notice: 'Property was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def can_sell (user)
      plan = user.subscription
      properties = user.properties
      forsale = 0
      # we need to scan properties and count those for sale
      properties.each do |prop|
        if prop.status.to_i == 1
          forsale += 1
        end
      end

      return plan.to_f > forsale
    end

    def get_percentage (missing_fields, total_fields)
      
      return (total_fields - missing_fields.count.to_f) / total_fields * 100
    end  

    def get_missing_fields (property)
      #byebug
      missing_fields = []

      fields_to_count = {}
      fields_to_count['basic'] = ['address', 'roof_type']
      fields_to_count['exclusive'] = {}
      fields_to_count['exclusive']['dallas'] = ['bedrooms', 'bathrooms']
      fields_to_count['exclusive']['austin'] = ['dining_area', 'kitchen']
      fields_to_count['exclusive']['huston'] = ['pool', 'garage']

      fields_to_count['basic'].each do |value|
        if property[value] == nil || property[value] == ''
          missing_fields.push(value)
        end
      end

      if fields_to_count['exclusive'][property.state] != nil
        fields_to_count['exclusive'][property.state].each do |value|

          if property[value] == nil || property[value] == ''
            missing_fields.push(value)
          end  
        end
      end

      return missing_fields
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_property
      @property = Property.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def property_params
      params.require(:property).permit(:state, :address, :roof_type, :bedrooms, :bathrooms, :dining_area, :kitchen, :pool, :garage, :status)
    end
end

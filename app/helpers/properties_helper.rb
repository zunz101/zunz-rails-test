module PropertiesHelper

	def user_can_edit(item) 
		return current_user.id  == item.user.id 
	end
end

json.extract! property, :id, :state, :address, :roof_type, :bedrooms, :bathrooms, :dining_area, :kitchen, :pool, :garage, :created_at, :updated_at
json.url property_url(property, format: :json)
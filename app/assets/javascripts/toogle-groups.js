(function(){
	jQuery.fn.toogleClass = function (cls, func) {
		var me = jQuery(this), 
			state = me.hasClass(cls),
			func = func || function() {}
		if (state) 
			me.removeClass(cls);
		else
			me.addClass(cls);

		func.call(this, state);
	}

	var handleGroups = function () {
		console.log(1);
		var toggle_groups = function(event, node) {
			var node = node? node : this;
			var group = jQuery(node).val().trim();
			
			jQuery('.field.group:not(.group-'+group).addClass('hidden');
			jQuery('.field.group-'+group).removeClass('hidden');
		}
		var state = jQuery('#property_state')
			.change(toggle_groups);
		
		if (state.length)
			toggle_groups(null, state.get(0));
	}

	jQuery(document).on('ready page:load page:change', handleGroups);
	// jQuery(document).on('turbolinks:load', handleGroups);
})(jQuery)
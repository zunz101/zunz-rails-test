class Property
  include Mongoid::Document
  field :state, type: String
  field :address, type: String
  field :roof_type, type: String
  field :bedrooms, type: Integer
  field :bathrooms, type: Integer
  field :dining_area, type: Mongoid::Boolean
  field :kitchen, type: Mongoid::Boolean
  field :pool, type: Mongoid::Boolean
  field :garage, type: Mongoid::Boolean

  field :status, type: Integer, default: 0

  validates :state, :address, :presence => true

  belongs_to :user

  #accepts_nested_attributes_for :user

  def self.can_edit(user)
    return self.user && self.user == user
  end

end
